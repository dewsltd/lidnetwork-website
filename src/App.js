import React, { Suspense } from 'react';
import { ErrorBoundary } from 'react-error-boundary';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Airdrop from './screens/Airdrop';
import API from './screens/API';
import ICO from './screens/ICO';

import LandingPage from './screens/LandingPage';
import WhitePaper from './screens/WhitePaper';

function App() {

  return (
    <React.Fragment>
      <Router>
        <ErrorBoundary FallbackComponent={<div>Loading</div>}>
          <Suspense>
            <Switch>
              <Route path="/" exact component={LandingPage} />
              <Route path="/white-paper" component={WhitePaper} />
              <Route path="/documentation" component={API} />
              <Route path="/ico" component={ICO} />
              <Route path="/airdrop" component={Airdrop} />
            </Switch>
          </Suspense>
        </ErrorBoundary>
      </Router>
    </React.Fragment>
  )
}

export default App;
