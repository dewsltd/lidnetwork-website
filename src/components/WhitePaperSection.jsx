import React from 'react'

const WhitePaperSection = ({ children, title }) => {
    return (
        <div className="w-full pb-32 items-center flex flex-col">
            <div className={`h-260 w-full bg_lids_green_light flex flex-col justify-end pb-8 pt-5`}>
                <img className="ml-4 md:ml-28 w-min" src='/assets/images/title_header.svg' alt="section header " />
                <h1 className="ml-4 md:ml-28 w-full md:w-8/12 xl:w-7/12 createdReason text_lids_white px-3 md:px-0 mt-3 md:mt-1">{title}</h1>

            </div>
            <div className="w-9/12 flex flex-col items-center mt-28">
                {children}
            </div>

        </div>
    )
}

export default WhitePaperSection
