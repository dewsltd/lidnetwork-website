import React from 'react'

const APICard = ({ api }) => {
    const { title, description, endpoint } = api
    return (
        <div className="my-20 flex flex-col items-center">
            <h1 className="w-full text_lids_gray text-3xl md:text-5xl 2xl:text-7xl 3xl:text-9xl font-semibold" >
                {title}
            </h1>
            <h3 className="w-full text-xl my-12 md:pr-10 white_paper_text">
                {description}
            </h3>

            <div className="api_box">
                <p className="text_lids_green_second italic">{endpoint}</p>
            </div>


        </div>
    )
}

export default APICard
