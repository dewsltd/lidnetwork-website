import React from 'react'
import Nav from './Nav'

const TopSection = () => {
    return (
        <div id="top_section" className="flex flex-col">
            <Nav title="Quicoin Project" />
            <div className="flex-1 flex flex-col md:flex-row items-center justify-center md:justify-start px-6 pt-16 md:pt-0 ">
                <h1 className="lg:w-8/12 pt-10 md:pt-0 text-3xl lg:text-5xl xl:text-6xl 3xl:text-9xl text-white top_title mr-4 md:pl-3 font-bold">Quicoin is a payment network, a secure and stable financial asset</h1>
                <div className="flex flex-col items-center">
                <img className="top_img w-7/12 ml-4" src="/assets/images/lidcoin.svg" alt="qui payment solution" />
                <img className="w-96" src="/assets/images/shadow.svg" alt="qui payment solution" />
                </div>
            </div>
        </div>
    )
}

export default TopSection
