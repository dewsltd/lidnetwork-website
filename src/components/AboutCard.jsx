import React from 'react'

const AboutCard = ({ item }) => {
    const { title, description, image } = item
    return (
        <div className="w-full md:w-1/3 flex justify-center md:justify-start items-center">
            <div className="w-2/3 about_card mb-10 p-6">
                <div className="flex">
                    <img className="w-8 h-8 mr-4" src={image} alt={title} />
                    <h3 className="text_size_30 mb-14 text-about font-semibold">{title}</h3>
                </div>
                <p className="text-lg text-about">{description}</p>
            </div>
        </div>
    )
}

export default AboutCard
