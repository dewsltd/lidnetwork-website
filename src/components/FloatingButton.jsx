import React from 'react'
import { Link } from 'react-router-dom'

const FloatingButton = () => {
    return (
        <Link to="/white-paper" className="fixed bottom-5 right-5 border rounded-full w-12 h-12 flex items-center justify-center text_lids_green border-green-300" >
            PDF
        </Link>
    )
}

export default FloatingButton
