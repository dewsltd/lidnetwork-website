import React, { useEffect } from 'react'
import { dilsApiCall } from '../utils/apiCall';
import { displayModalError } from '../utils/helpers';

const PayPalButton = ({
    amount, currency,
    qcAmount, walletAddress,
    setShowModal,
}) => {

    useEffect(() => {
        window.paypal
            .Buttons({
                style: {
                    layout: 'vertical',
                    color: 'blue',
                    shape: 'pill', //rect
                    label: 'paypal'
                },
                createOrder: (data, actions) => {
                    displayModalError("", setShowModal, null, "")
                    // console.log('DATA CREATE ORDER: ', data)
                    // console.log('ACTIONS CREATE ORDER: ', actions)
                    return actions.order.create({
                        intent: "CAPTURE",
                        purchase_units: [
                            {
                                description: `Payment for ${currency}${amount} of Quicoin`,
                                amount: {
                                    currency_code: currency,
                                    value: amount,
                                },
                            },
                        ],
                        application_context: {
                            shipping_preference: "NO_SHIPPING",
                        }
                    });
                },
                onApprove: async (data, actions) => {
                    // console.log('DATA APPROVE ORDER: ', data)
                    // console.log('ACTIONS APPROVE ORDER: ', actions)
                    const order = await actions.order.capture();
                    // console.log('ORDER DETAIL: ',order);
                    const requestModal = { payment: order, coin: qcAmount, walletAddress }
                    const networkRequest = await dilsApiCall.post(`callbacks/paypal-confirm`, requestModal);
                    if (networkRequest?.status === "success") {
                        setShowModal((oldState) => ({
                            ...oldState,
                            error: "",
                            tab: 2,
                        }))
                        return;
                    }
                    displayModalError(networkRequest, setShowModal, null, "Error validating payment. Contact support")
                },
                onError: (err) => {
                    displayModalError(err, setShowModal, null, "Error Processing payment")
                    console.error('PAYMENT ERROR IS: ', err);
                },
            })
            .render('#paypal-button-container');
    }, [amount, currency, qcAmount, setShowModal, walletAddress]);

    return (
        <div>
        </div>
    )
}

export default PayPalButton
