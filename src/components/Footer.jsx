import React from 'react'
import { SOCIAL_CONTACT } from '../utils/constants'

const Footer = () => {
    return (
        <div id="footer-section" className="px-10 md:px-20 text-white md:flex justify-between py-8">
            <div>
            <img className="" src="/assets/images/logo.svg" alt="developer documentation" />
            </div>
            
            <div className="md:flex">
                <h3 className="text-black mr-6 font-bold text-md mb-3 text-start mt-12 md:mt-0">Follow Us</h3>
                <div className="flex justify-start md:justify-center">
                    {SOCIAL_CONTACT.socialMedia.map(platform => (
                        <a key={platform.name} href={platform.url}><img src={platform.icon} alt={platform.name} className="w-5 mr-5" /></a>
                    ))}
                </div>
            </div>
        </div>
    )
}

export default Footer
