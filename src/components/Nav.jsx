import React, { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import MobileNav from './MobileNav';
import { Link as ReactScroll } from "react-scroll";


const Nav = ({ title }) => {
    const [openNav, setOpenNav] = useState(false);

    const history = useHistory();
    const { pathname } = history.location;

    return (
        <React.Fragment>
            <nav className="fixed md:static top-0 nav_style flex justify-between items-center w-full">
                <Link className="cursor-pointer flex items-center" to="/">
                    <img
                        className="ml-3"
                        src="/assets/images/logo.svg"
                        alt="QuiCoinNetwork logo"
                    />
                    <h2 className="text-xl nav_text ml-7">{title ?? ''}</h2>
                </Link>

                <div className="hidden md:flex items-center">
                    {
                        pathname === "/"
                            ?
                            <ReactScroll
                                smooth={true}
                                offset={-200}
                                duration={500}
                                className="text-about text-md lg:text-lg 2xl:text-xl 3xl:text-4xl mr_60 cursor-pointer"
                                to="about">
                                About
                    </ReactScroll>
                            :
                            <Link to="/" className="cursor-pointer text-about text-md lg:text-lg 2xl:text-xl 3xl:text-4xl mr_60 cursor-pointer">About</Link>
                    }
                    <Link to="/documentation" className="cursor-pointer text-about text-md lg:text-lg 2xl:text-xl 3xl:text-4xl  mr_60">Developers</Link>
                    {/* <Link to="/airdrop" className="cursor-pointer text-about text-md lg:text-lg 2xl:text-xl 3xl:text-4xl  mr_60">Airdrop</Link> */}
                    {/* border-2  */}
                    <Link to="/ico" className="cursor-pointer text-about text-md lg:text-lg 2xl:text-xl 3xl:text-4xl mr_60 border rounded-xl rounded-3xl px-8 py-2">ICO</Link>

                    {/* <Link to="/white-paper" className="bg_lids_green_deep text-white text-md lg:text-lg 2xl:text-xl 3xl:text-4xl rounded-full mr_60 px-5 py-3">Whitepaper</Link> */}

                    <a 
                    rel="noreferrer" 
                    target="_blank" 
                    href="/assets/pdf/quicoin_whitepaper.pdf" 
                    download 
                    className="cursor-pointer flex bg_lids_green_light text-white text-md lg:text-lg 2xl:text-xl 3xl:text-4xl rounded-full mr_60 px-6 py-2 items-center">
                        Whitepaper &nbsp;
                        <img className="w-10 m-0 m_0 px-1" src="/assets/images/download.svg" alt="download"/>
                    </a>
                </div>
                <img
                    onClick={() => setOpenNav(!openNav)}
                    className="cursor-pointer md:hidden mr-4"
                    src={`${openNav === true ? "/assets/images/close.svg" : "/assets/images/hamburger.svg"}`}
                    alt="drop down options"
                />
            </nav>
            {openNav && <MobileNav setOpenNav={setOpenNav} />
            }
        </React.Fragment>

    )
}

export default Nav
