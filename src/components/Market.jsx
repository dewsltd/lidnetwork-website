import React from 'react'

const Market = () => {
    return (
        <div className="flex flex-col md:flex-row py-20 bg_lids_gray">
            <div className="w-full md:w-1/2 p-10 md:p-20 flex flex-col justify-center">
                <h1 className="text_lids_green text_size_35 text-left">Market</h1>
                <p className="text-black text-md lg:text-xl 2xl:text-3xl 3xl:text-5xl w-10/12 mt-5 mb-10">Quicoin can be bought on dils trading platform. Dils is a p2p market place that gives users control and privacy.</p>
                <img src="/assets/images/dils.svg" className="dils_logo mb-14" alt="dils app" />
                <a href="http://www.dils.uk" target="_blank" rel="noopener noreferrer" className="shop_btn text-white py-4 px-5">Shop Now</a>
            </div>
            <div className="w-full md:w-1/2 md:p-20 flex justify-center md:justify-end">
            <img src="/assets/images/market.svg" className="w-10/12 md:w-1/2 white_paper_book" alt="market for dils app" />
            </div>
        </div>
    )
}

export default Market
