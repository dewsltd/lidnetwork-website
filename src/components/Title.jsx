import React from 'react';
import "./Title.css"

const Title = ({ text, backgroundColor, color, width, otherClasses }) => {
    return (
        <div className={`relative z-30 mb-10 ${otherClasses ?? ''}`} style={{width: width + 20 ?? 100 + 20, height: 45}}>
            <div className="absolute title_bg" style={{backgroundColor: backgroundColor ?? "green", width: width ?? 100}}></div>
            <h1 className="text-3xl font-bold ml-3" style={{color: color ?? "#000"}} >{text}</h1>
            <div />
        </div>
    )
}
export default Title;
