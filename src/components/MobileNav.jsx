import React from 'react'
import { Link } from 'react-router-dom'
import { allLinks } from '../utils/data'

const MobileNav = ({ setOpenNav }) => {
    return (
        <div className="fixed bg_lids_green_deep top-14 bottom-0 left-0 right-0 z-40 h-screen flex flex-col" >
            <div className="flex-1 text-center flex flex-col justify-center align-center">
                {allLinks.map(link => (
                     <Link
                     key={link.title}
                     onClick={() => setOpenNav(false)}
                     className="text-white py-5 cursor-pointer font-bold text-2xl"
                     to={link.to}>
                     {link.title}
                    </Link>
                ))}
            </div>
        </div>
    )
}

export default MobileNav
