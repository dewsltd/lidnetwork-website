import React, { Fragment } from 'react'

const options = [
    {
        id: 0,
        title: "Create Wallet"
    },
    {
        id: 1,
        title: "Payment Information"
    },
    {
        id: 2,
        title: "Finish"
    },
]

const StepperHeader = ({currentIndex}) => {
    return (
        <Fragment>
            <div className="line hidden md:block"></div>
        <div className="stepper_header flex flex-col md:flex-row justify-center md:justify-between px-10">
            {
                options.map(item => (
                    <div key={item.id} className="stepper flex md:flex-col  md:justify-center items-center">
                        <div className={`w-2 md:w-3 h-2 md:h-3 rounded-2xl z-50 ${currentIndex === item.id ? "bg_lids_green_light" : "bg_stepper_gray"} `} />
                        <p className={`text-sm ml-1 md:mt-2 ${currentIndex === item.id ? "text_lids_green_second" : "text_stepper_gray" } `}>{item.title}</p>
                    </div>
                ))
            }
        </div>
        </Fragment>
    )
}

export default StepperHeader
