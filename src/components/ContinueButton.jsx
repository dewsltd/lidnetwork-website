import React from 'react'

const ContinueButton = ({ text, onPress, className, isLoading }) => {
    return (
        <>
            {
                isLoading === true ?
                    <div className="w-10">
                        <div className="loader">Loading...</div>
                    </div>
                    :
                    <button onClick={onPress} className={`roboto continue_btn outline-none cursor-pointer ${className}`}>
                        {text}
                    </button>

            }
        </>
    )
}

export default ContinueButton
