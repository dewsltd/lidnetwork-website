import React, { Fragment, useState } from 'react'

const FormInput = ({
    label,
    type,
    onChange,
    error,
    value,
    allCurrencies,
    onCurrencyChange,
    isOutlineInput,
    copyBtn,
    className
}) => {

    const [copyText, setCopyText] = useState(false)
    const handleWalletAddressCopy = () => {
        navigator.clipboard.writeText(value);
        setCopyText(true)
        setTimeout(() => setCopyText(false), 1000)
    }
    return (
        <Fragment>
            <div className={`mb-5 ${isOutlineInput === true ? "w-full" : "w-10/12 md:w-1/3 ml-3 mr-6"} ${className ?? ''}`}>
                <p className={`roboto py-1 ${copyBtn === true ? "text_form_gray text-sm font-medium" : isOutlineInput === true ? "text_form_gray text-sm font-medium" : "subText mb-3"}`}>{label}</p>
                <div className={`w-full overflow-hidden h-12 rounded-md flex ${copyBtn === true ? "border_outline_green" : isOutlineInput === true ? "" : "form_input"}`}>
                    <input
                        disabled={copyBtn === true}
                        onChange={onChange}
                        type={type ?? "text"}
                        // pattern="\d*[,.]\d+|\d+(?:[,.]\d*)?"
                        // lang="nb"
                        value={value}
                        className={`w-full text_form_gray font-light h-12 rounded-md px-4 roboto ${copyBtn === true ? "" : isOutlineInput === true ? "border border_outline_green" : "form_input"}`}
                    />
                    {
                        copyBtn === true && (
                            <div
                                onClick={handleWalletAddressCopy}
                                className="cursor-pointer h-full pl-2 pr-3 bg_lids_green flex justify-center items-center">
                                <img src="/assets/images/copy.svg" alt="copy button" />
                            </div>
                        )
                    }
                    {
                        (allCurrencies && allCurrencies.length > 0) && (
                            <select
                                className="form_input mr-2"
                                onChange={onCurrencyChange}
                            >
                                {allCurrencies.map(curr => (
                                    <option key={curr.value} value={curr.value}>{curr.value}</option>
                                ))}
                            </select>
                        )
                    }
                </div>
                <p className="mt-1 text-xs text-red-500">{error}</p>
            </div>

            {(copyText === true && copyBtn === true) && 
            <div className="absolute bottom-3 left-0 right-0 flex justify-center" >
                <p className="text-sm bg-gray-600 text-white px-5 py-1 rounded-2xl">Copied</p>
            </div>}
        </Fragment>
    )
}

export default FormInput
