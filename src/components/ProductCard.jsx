import React from 'react'

const ProductCard = ({product, type}) => {
    const { title, image, description } = product
    return (
        <div className={`mb-10 ${type === 'reason' ? 'w-full md:w-1/2' : 'w-full md:w-1/3'}`}>
            <img src={image} alt={title} className="md:w-14 lg:w-20 xl:w-20 md:h-14 lg:h-20 xl:h-20"/>
            <h1 className="text-black my-3 text-2xl lg:text-3xl 2xl:text-5xl 3xl:text-8xl">{title}</h1>
            <p className={`max-w-full text-black ${type === 'reason' ? 'w-full md:w-7/12' : 'w-full md:w-64 2xl:w-96'} text-md lg:text-2xl 2xl:text-2xl 3xl:text-7xl`}>{description}</p> 
        </div>
    )
}

export default ProductCard
