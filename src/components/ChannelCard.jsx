import React from 'react'

const ChannelCard = ({ channelInfo }) => {
    const { image, title, description, link } = channelInfo
    return (
        <div className="flex mb-10 ">
            <img className="developer_network_icon mr-3" src={image} alt={title} />
            <div className="flex flex-col">
                <a rel="noreferrer" target="_blank" href={link}  className="text_lids_green text-lg lg:text-2xl 2xl:text-5xl 3xl:text-7xl">{title}</a>
                <p className="text-black text-md lg:text-xl 2xl:text-3xl 3xl:text-5xl w-10/12">
                    {description}
                </p>
            </div>
        </div>
    )
}

export default ChannelCard
