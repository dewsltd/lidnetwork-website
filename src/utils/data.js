export const allTeamMembers = [
    { name: "David Whyte", position: "---", image: "/assets/images/team.png" },
    { name: "Francis Sunday", position: "---", image: "/assets/images/team.png" },
    { name: "Odunewu Iyanuoluwa", position: "UI/UX", image: "/assets/images/team.png" },
    { name: "Soji Ologbenla", position: "---", image: "/assets/images/team.png" },
    { name: "Suulola Oluwaseyi", position: "Software Developer", image: "/assets/images/team.png" },
] 

export const features = [
    {
      title: "Fast",
      image: "/assets/images/fast.svg",
      description: "The Quicoin network was built to be fast, move limitless amount of Quicoin in seconds"
    },
    {
      title: "Stable",
      image: "/assets/images/stable.svg",
      description: "Quicoin is the most stable digital asset today, it employs the wine pricing model, not the conventional demand and supply model alone."
    },
    {
      title: "Secured",
      image: "/assets/images/secure.svg",
      description: "Quicoin network was built to be secure"
    },
    {
      title: "Scalable",
      image: "/assets/images/scalable.svg",
      description: "The Quicoin network was built to handle billions of transactions"
    },
    {
      title: "Accessible",
      image: "/assets/images/accessible.svg",
      description: "Anyone with a computer, smartphone, or access to the internet can use the system."
    },
  ]

  export const allLinks = [
    { to: "/", title: "Home" },
    { to: "/", title: "About" },
    { to: "/documentation", title: "Developer" },
    { to: "/white-paper", title: "White Paper" },
    { to: "/ico", title: "ICO" },
    // { to: "/airdrop", title: "Airdrop" },
]

export const developerNetwork = [
  { 
    title: "Join the Telegram community", 
    link: "https://t.me/joinchat/Rcq3QoXLmzQwMmZk",
    image: "/assets/images/telegram.svg",
    description: "The Telegram community is a place where we get to talk about development goals, contribute to the project, test software, bring up new technical and philosophical ideas." 
  },
  { 
    title: "Contribute to the project (GitLab)", 
    link: "https://gitlab.com/dewsltd/LID-server",
    image: "/assets/images/gitlab.svg",
    description: "" 
  },
]

export const topReasons = [
  {
    title: "Financial Inclusion",
    image: "/assets/images/inclusion.svg",
    description: "Everyone should have access to a payment system. This will help"
  },
  {
    title: "Global Payment",
    image: "/assets/images/global_payment.svg",
    description: "A system that facilitates global business transactions."
  },
]

export const aboutLID = [
  {
    title: "A distributed ledger", 
    description: "Quicoin network is an open space for computers to interact, process, and store data", 
    image: "/assets/images/ledger.svg",
    backgroundColor: "#8C3C1F",
  },
  {
    title: "A stable financial asset", 
    description: "Quicoin is the most stable cryptocurrency today", 
    image: "/assets/images/stable_asset.svg",
    backgroundColor: "#5063D9",
  },
  {
    title: "A payment system", 
    description: "The Quicoin network provides a decentralized infrastructure for peer to peer payment. No censorship, no extra fees", 
    image: "/assets/images/payment.svg",
    backgroundColor: "#8AC760",
  },
  {
    title: "Financial services for everyone", 
    description: "Financial inclusion is a huge problem with the world’s current economic system. We want to give access to everyone. Money and financial services are necessary for survival, everyone should have them.", 
    image: "/assets/images/inclusion_system.svg",
    backgroundColor: "#8C3C1F",
  },
  {
    title: "Ownership and privacy", 
    description: "Quicoin gives you complete anonymity, you own your digital asset, nobody can take it from you", 
    image: "/assets/images/ownership.svg",
    backgroundColor: "#5063D9",
  },
  {
    title: "Secure", 
    description: "Quicoin is currently the most secure digital asset on earth. We strike a balance between openness and security. Computers on the network are managed by an open community.", 
    image: "/assets/images/secured_blue.svg",
    backgroundColor: "#8AC760",
  },
  
]

export const documentationListings = [
  {
    title: "Create a wallet",
    description: "You can create a wallet on the network, by calling the create wallet API, passing in the name of the wallet or an empty string leaves the system to create a wallet with a random name",
    endpoint: "{host}/api/v1/wallet/create",
  },
  {
    title: "Check if wallet exists",
    description: "When checking or initiating transactions, you may need to check if a particular wallet exists.",
    endpoint: "{host}/api/v1/wallet/check",
  },
  {
    title: "Check wallet balance",
    description: "Check how many Quicoin exist in a wallet",
    endpoint: "{host}/api/v1/wallet/balance",
  },
  {
    title: "Transfer Quicoin from one wallet to another",
    description: "Move limitless amount of Quicoin securely from one wallet to another",
    endpoint: "{host}/api/v1/wallet/transfer",
  },
  
]