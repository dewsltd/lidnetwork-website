export const APP_GREEN = "#8BC860";
export const APP_BLACK = "#141719";
export const APP_WHITE = "#FCFFFB";
export const SOCIAL_CONTACT = {
    phone: "08139472050",
    email: "info@dewstech.com",
    address: "No. 13, Dugbe roundabout Ringroad, Ibadan",
    socialMedia: [
        {icon: "/assets/images/facebook.svg", name: "Facebook", url: "/"},
        {icon: "/assets/images/twitter.svg", name: "Twitter", url: "https://twitter.com/Dilsmarket"},
        {icon: "/assets/images/youtube.svg", name: "YouTube", url: "/"},
    ]
};