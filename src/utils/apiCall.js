const testBlockURL = `https://thirsty-wescoff-6627aa.netlify.app/api/blockchain`;
const testDilsURL = `https://thirsty-wescoff-6627aa.netlify.app/api/dils`;

// const testBlockURL = `http://157.245.88.82/api/v1`;
// const productionBlockchainURL = "http://157.230.10.241/api/v1"

// const testDilsURL = `https://dils-backend.herokuapp.com/api/v1`;
// const productionDilsURL = "http://68.183.131.208/api/v1"

export const dilsApiCall = {
    async get(url, request) {
        try {
            const networkCall = await fetch(`${testDilsURL}/${url}`, request)
            const response = await networkCall.json();
            return response
        } catch (error) {
            return error
        }
    },
    async post(url, request) {
        try {
            const networkCall = await fetch(`${testDilsURL}/${url}`, {
                method: "POST",
                body: JSON.stringify(request),
                headers: { "Content-type": "application/json;charset=UTF-8" }
            })
            const response = await networkCall.json();
            return response
        } catch (error) {
            return error
        }
    }
};
export const blockchainApiCall = {
    async get(url, request) {
        try {
            const networkCall = await fetch(`${testBlockURL}/${url}`, request)
            const response = await networkCall.json();
            return response
        } catch (error) {
            return error
        }
    },
    async post(url, request) {
        try {
            const networkCall = await fetch(`${testBlockURL}/${url}`, {
                method: "POST",
                body: JSON.stringify(request),
                headers: { "Content-type": "application/json;charset=UTF-8" }
            })
            const response = await networkCall.json();
            return response
        } catch (error) {
            return error
        }
    }
};

export const generalApiCall = {
    async get(url, request) {
        try {
            const networkCall = await fetch(`${url}`, request)
            const response = await networkCall.json();
            return response
        } catch (error) {
            return error
        }
    },
    async post(url, request) {
        try {
            const networkCall = await fetch(`${url}`, {
                method: "POST",
                body: JSON.stringify(request),
                headers: { "Content-type": "application/json;charset=UTF-8" }
            })
            const response = await networkCall.json();
            return response
        } catch (error) {
            return error
        }
    }
};

