export const logError = (
   error,
   setGeneralError,
   setIsLoading,
   errorMessage,
) => {
   setIsLoading(false)
   setGeneralError(error?.message ?? error?.data?.error ?? errorMessage ?? "Network Error. Please try again");
}

export const displayModalError = (
   error, 
   setShowModal, 
   setIsLoading, 
   errorMessage
   ) => {
   if (setIsLoading) {
      setIsLoading(false)
   }
   setShowModal((oldState) => ({
      ...oldState,
      error: error?.message ?? error?.data?.error ?? errorMessage,
   }))
}