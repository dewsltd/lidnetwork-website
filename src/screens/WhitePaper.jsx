import React from 'react'
import Nav from '../components/Nav'
import WhitePaperSection from '../components/WhitePaperSection'

const WhitePaper = () => {
    return (
        <div className="">
            <Nav />
            <div className="w-full px-10">
                <div className="mt-10 white_paper_title flex flex-col items-center justify-between overflow-hidden">
                    <h1 className="w-full md:w-2/3 pt-20 createdReason white_paper_text">Quicoin Cryptocurrency white paper</h1>
                    <div className="md:ml-40 w-full flex justify-end md:justify-start">
                        <img className="ml-5 hidden md:block" src="/assets/images/scroll.svg" alt="scroll to read" />
                        <small className="ml-2 text_lids_gray text-sm self-end md:self-start md:mt-1">Scroll to read</small>

                    </div>
                </div>
            </div>
            <WhitePaperSection title="Introduction">
                <p className="white_paper_text white_paper_p">
                    This document will be divided into 3 parts, the technical, philosophical, and future plans.
                    Quicoin is a cryptocurrency that is futuristic and social in nature.
                    We want to push the cryptocurrency ideology to another level.
                    We aim at solving the pressing issues the crypto world faces.
                    Adoption, capital, exchange, stability, scalability, integrability, anonymity, 
                    and zero government interference are the values that we uphold.
                    </p>
            </WhitePaperSection>

            <WhitePaperSection title="Quicoin Technicalities">
                <h3 className="text-3xl w-full mb-5">Stack and technologies</h3>
                <p className="white_paper_text white_paper_p">
                    We built Quicoin with Golang, it is going to become an open-source project in the future.
                    It will have documentation that is easy to read and understand.
                    We chose Golang because of its scalability, speed, and wide adoption in backend service development.
                    We will be working with the HTTP Rest protocol because we feel that this system has a long history and foundation,
                    wide adoption and it will stand even in the future.
                    With research in AI, Quantum, and Photonic computing, it can become uncertain what future technologies will look like.
                    We still believe that this internet protocol will stand, and maybe be modified.
                    We will however keep updating our technology as demand increases.
                </p>

                <h3 className="text-3xl w-full mb-5 mt-14">Architecture</h3>
                <p className="white_paper_text white_paper_p">
                    Quicoin has a secure architecture that is fault-tolerant.
                    The main things we are looking out for when developing our software and hardware technology are:
                    </p>

                <ul className="w-10/12 my-10">
                    <li className="list-disc"> Speed</li>
                    <li className="list-disc">Scalability </li>
                    <li className="list-disc">Fault tolerance </li>
                </ul>
                <p className="white_paper_text italic">
                    There are two types of servers on the Quicoin network, the processing, and archive servers.
                    The Quicoin network consists of computers running the Quicoin server software, exchanging information 
                    together to execute the system goals.
                    </p>

                <h3 className="text-3xl w-full mb-5 mt-14">Processing servers</h3>
                <p className="white_paper_text white_paper_p">
                    This type of server makes up the majority of servers in the network.
                    They take in requests from users over an HTTP connection and broadcasts the actions and transactions with other servers.
                    When a user sends in a transaction stating that he is sending 10 QuiCoins for example to wallet “xxxxxx”,
                    the initial node(the node that is in communication with a user device) processes this transaction, validates it,
                    and sends the transaction data to other processing nodes to do the same.
                    Every processing server on the network randomly selects a defined number of nodes to connect to.
                    This random selection makes sure that at all times all the nodes on the network get updated.
                    </p>

                <p className="white_paper_text italic pt-10">
                    The servers on the network have resistance to crashes by design.
                    If a node goes offline for some reason and comes back online after a few days,
                    it automatically picks up from where it left off and updates itself.
                </p>

                <h3 className="text-3xl w-full mb-5 mt-14">Archive servers</h3>
                <p className="white_paper_text white_paper_p">
                    These servers are just like Processing servers, 
                    the only difference is that they have the memory management feature disabled, so they never lose any data. 
                    They could store data for years and even decades.
                </p>
            </WhitePaperSection>
            <WhitePaperSection title="How do we manage storage resources?">
                <p className="white_paper_text white_paper_p">
                    Nodes on the network do not need much storage space. 
                    There is a memory management feature built into each server, 
                    and what happens is that data (blocks) is deleted automatically from each wallet, 
                    only data that is needed by the system persists.
                </p>
            </WhitePaperSection>
            <WhitePaperSection title="How do transactions work?">
                <p className="white_paper_text white_paper_p">
                    With the Quicoin cryptocurrency system, users can create wallets, 
                    and an initial block is added to it and then it is propagated on all servers. 
                    A transaction is basically an instruction set. 
                    A transaction instructs the server to add a new block with XXX data into the chain which is in turn attached to a wallet.
                    To add a new block to a chain, a request is sent to a random server and the server validates the block, the address, 
                    and the previous block. Then the server sends this new transaction to other random servers in the network. 
                    These servers do the same validation and send it to other servers again in the network.
                </p>
                <h3 className="text-3xl w-full mb-5 mt-14">How do we handle too many requests and the speed of transactions?</h3>
                <p className="white_paper_text white_paper_p mb-10">
                    We have a way of handling this issue. In the blockchain tech space, this has always been a major issue. 
                    It could take some time for a transaction to make a round trip to every server. Many networks have over 5,000 servers. 
                    Having many servers is a good thing, it increases the level of trust in the system.
                </p>
                <p className="white_paper_text white_paper_p mb-10">
                    When a transaction occurs on the qui network, the validation is done on the initial node 
                    and 1-3% of the network and a response is sent. 
                    What this does is create a seamless instant experience for the user. In the background, 
                    servers are broadcasting the transactions to other servers. 
                    A server on the network may broadcast to a random 2% of the whole network. 
                    This eliminates the issue of a round trip, probably thousands of times, now we only broadcast 5-10 times. 
                    Practically we may broadcast transactions many times but a server does not process a transaction 
                    it has processed before, we have a way of identifying each transaction.
                </p>
                <p className="white_paper_text white_paper_p mb-10">
                    We have security features built into each server to resist a DDOS attack. 
                    The network as a whole is built to work properly even when multiple servers fail 
                    and when servers are added they get synchronized easily.
                </p>
                <p className="white_paper_text white_paper_p">
                    We will be starting out with about 3 nodes at the ICO. 
                    We should scale up to 10 after the ICO and then 20 after 1 year. 
                    Our goal is to reach 100 servers in 2 years. With our architecture, 
                    adding more servers does not slow down the system, however, we do not need too many servers on the network. 
                    What we need are a few actively maintained servers that are monitored. 
                    100 processing nodes is a good number to target and maintain.
                </p>
            </WhitePaperSection>
            <WhitePaperSection title="How do we handle too many request and speed of transactions?">
                <p className="white_paper_text white_paper_p mb-10">
                    The process of adding a new server to the network is simple for us, 
                    we simply have to let all other servers in the network know about this new server. 
                    There can be no alien, anonymous or dormant servers on the network. 
                    The server software keeps track of the up and downtimes, 
                    this helps us to maintain the quality of the network.
                </p>
                <p className="white_paper_text white_paper_p">
                    People who are interested in running a Quicoin server can apply to join the network. 
                    If accepted they will be responsible for updating and maintaining their server. 
                    The qui networks community does the approving or disapproving of an individual or institution 
                    proposing to run the qui server. These are the criteria for joining the network:
                </p>
                <ul className="my-10 w-10/12">
                    <li className="list-disc">You have a computer with at least 2gig of ram.</li>
                    <li className="list-disc">This computer is dedicated to running the qui node</li>
                    <li className="list-disc">The node maintains an 80% uptime</li>
                    <li className="list-disc">You make efforts to prevent viruses or malware (we advise using a fresh OS installation)</li>
                    <li className="list-disc">You stay active in the community space and update server software when released.</li>

                </ul>
                <p className="white_paper_text white_paper_p">
                    There will be an open community of people running the qui servers. When someone is not actively maintaining 
                    his server and there are multiple reports of malfunction, the community can remove that person from 
                    the network and add another person.
                </p>
            </WhitePaperSection>
            <WhitePaperSection title="Philosophy">
                <p className="white_paper_text white_paper_p mb-10">
                    In this section, we get to discuss what the qui asset is and how it should be used and what makes 
                    it the best asset today.
                </p>
                <p className="white_paper_text white_paper_p">
                    In the beginning, there will be a static amount of Quicoin (1 billion) at a static exchange price (1$). 
                    Every time you buy a Quicoin or carry out a transaction, 0.001 Quicoin is created, 10% of this residue Quicoin 
                    is given to you (who initiated the transaction), 30% is given to the miners, 60% is stored in the Quicoin bank. 
                    The qui network needs this supply of Quicoin to keep its operations and its community running. 
                    Softwares need upgrades, hardwares, need improvement, security maintenance, and other missions of the community.
                </p>
            </WhitePaperSection>
            <WhitePaperSection title="How is price determined?">
                <p className="white_paper_text white_paper_p">
                    The price of the Quicoin is not totally determined by supply and demand. 
                    The price changes every year, and it only increases, the qui’s price is determined mainly by time. 
                    The price increases by 100% every year. The price doesn’t fall in an economic meltdown. 
                    This stability makes it the best investment on earth today. 
                    This price principle is called the Wine asset principle, developed by a group of economists around 
                    the world looking to change the way the financial system works. 
                    A special class of assets immune to the forces of demand and supply and keeps appreciating over time. 
                    This is automatically built into Quicoin software and allows for an automated pricing system.
                </p>
            </WhitePaperSection>
            <WhitePaperSection title="Integration">
                <p className="white_paper_text white_paper_p">
                We are building Quicoin to last for years without any major updates. 
                We are building with integration in mind. 
                Other companies will be able to accept Quicoin as money, 
                and there will be applications and software built to work with qui crypto. 
                Software developers can integrate the qui platform into what they are building.
                </p>
            </WhitePaperSection>
            <WhitePaperSection title="The future">
                <p className="white_paper_text white_paper_p">
                We believe in Nationalism and diversity. 
                We also believe in an open world, where nations trade with other nations and freedom of money. 
                In the future Quicoin and many other cryptocurrencies will be used to trade across Africa and beyond. 
                Investors will take advantage of Quicoin appreciative nature and stability to invest and grow wealth. 
                We will be able to alleviate people from poverty and enforce the truth that everyone has a right to money. 
                We believe in privacy and building businesses, 
                we also believe everyone should have money and not be at a disadvantage due to location, 
                politics, gender, race or age. Quicoin will be managed by an independent private company monitoring 
                the many servers that are online. When software updates need to be made, 
                this company will also be responsible for making the necessary changes. 
                </p>
            </WhitePaperSection>
            
        </div>
    )
}

export default WhitePaper
