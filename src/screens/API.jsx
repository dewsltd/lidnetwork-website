import React from 'react'
import Nav from '../components/Nav'
import { documentationListings } from '../utils/data'
import APICard from '../components/APICard';

const API = () => {
    return (
        <div className="flex flex-col items-center">
            <Nav title="api" />
            <div className="px-5 md:px-20 w-full md:w-10/12">
            <h1 className="w-full md:w-2/3 pt-20 createdReason text-black">Quicoin API</h1>
            <h3 className="text-xl my-4">Build products with Quicoin as a payment option.</h3>
            <h3 className="text-xl mb-32">Users want to pay with Quicoin, is your company accepting Quicoin?</h3>

            <div>
                {documentationListings.map(api => (
                    <APICard 
                    key={api.title}
                    api={api}
                    />
                ))}
            </div>

            </div>
            
        </div>
    )
}

export default API
