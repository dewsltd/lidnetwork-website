import React, { useState, useEffect, } from 'react'
import FormInput from '../components/FormInput'
import Nav from '../components/Nav'
import { dilsApiCall, blockchainApiCall, generalApiCall } from "../utils/apiCall";
import StepperHeader from '../components/StepperHeader'
import CreateWallet from './stepper/CreateWallet'
import PaymentInformation from './stepper/PaymentInformation'
import Finish from './stepper/Finish';

import "./css/ICO.scss"

const handleErrorMessageSetting = (errorMessage, handler) => {
    handler((oldState) => ({
        ...oldState,
        error: errorMessage,
    }))
}


const ICO = () => {

    const [showModal, setShowModal] = useState({ value: false, tab: 0, walletAddress: "", error: "" });

    const [currencyRates, setCurrencyRates] = useState({})
    const [blockChainPrice, setBlockChainPrice] = useState(0)
    const [allCurrencies, setAllCurrencies] = useState([
        {
            "value": "USD",
            "name": "United States Dollar"
        },
        {
            "value": "NGN",
            "name": "Nigerian Naira"
        }]);

    const [amountOfCoin, setAmountOfCoin] = useState({ value: "1", error: "" });
    const [currencyEquivalent, setCurrencyEquivalent] = useState({ currency: "USD", amount: "0", error: "", dollarValue: "" });

    const fetchAllCurrencies = async () => {
        try {
            const networkRequest = await dilsApiCall.get(`currencies`);
            if (networkRequest.status === "success" && networkRequest.data) {
                networkRequest.data.sort((a, b) => a.name > b.name) //.unshift({ name: "Select Currency", value: "" });
                setAllCurrencies(networkRequest.data);
            }
        } catch (error) {
            console.log('FETCH ALL CURRENCIES ERROR', error)
        }
    }

    const fetchCurrentConversionRate = async () => {
        try {
            const networkRequest = await generalApiCall.get("https://thirsty-wescoff-6627aa.netlify.app/api/fixer");
            // const networkRequest = await generalApiCall.get("http://data.fixer.io/api/latest?base=EUR&access_key=d1e0591188b3fc3924f9be562cab89a3&format=1&symbols=JPY,SGD,GEL,GHS,GGP,EUR,GBP,AUD,NGN,CHF,USD");
            if (networkRequest?.rates) {
                setCurrencyRates(networkRequest.rates);
                return;
            }
        } catch (error) {
            console.log(error, "CURRENCY RATE ERROR")
        }
    }

    const fetchBlockChainPrice = async () => {
        try {
            const networkRequest = await blockchainApiCall.get("price");
            console.log("BLOCK CHAIN PRICE", networkRequest)
            if (networkRequest?.data?.current_price) {
                setBlockChainPrice(networkRequest?.data?.current_price);
                setCurrencyEquivalent((oldState) => ({
                    ...oldState,
                    amount: `${networkRequest?.data?.current_price}`,
                }))
                return;
            } else {
                //DELETE
                setBlockChainPrice("2")
                setCurrencyEquivalent((oldState) => ({
                    ...oldState,
                    amount: "2",
                }))
                //DELETE
            }
            console.log(networkRequest?.message ?? "Error not defined", "FIRST BLOCK CHAIN PRICE ERROR")
            // SHOW ERROR MESSAGE OF NETWORK.MESSAGE
        } catch (error) {
            console.log(error, "BLOCK CHAIN PRICE ERROR")
        }
    }

    useEffect(() => {
        fetchBlockChainPrice()
        fetchAllCurrencies()
        fetchCurrentConversionRate()
    }, [])

    const onCurrencyChange = (e) => {
        try {
            setCurrencyEquivalent((oldState) => ({
                ...oldState,
                currency: e.target.value,
            }))
            if (currencyRates['USD'] && currencyRates[e.target.value]) {
                const calculatedValue = (blockChainPrice * (currencyRates[e.target.value] / currencyRates['USD'])) * + amountOfCoin.value;
                const dollarValue = (blockChainPrice * (currencyRates['USD'] / currencyRates['USD'])) * + amountOfCoin.value;
                setCurrencyEquivalent((oldState) => ({
                    ...oldState,
                    dollarValue,
                    amount: `${(calculatedValue)}`,
                }))
            }
        } catch (error) {
            console.log('ON CURRENCY CHANGE', error)
        }
    }

    const handleCurrencyEquivalentAmount = (e) => {
        setCurrencyEquivalent((oldState) => ({
            ...oldState,
            amount: e.target.value,
        }));

        if (currencyRates['USD'] && currencyRates[currencyEquivalent.currency]) {
            const calculatedValue = e.target.value / (blockChainPrice * (currencyRates[currencyEquivalent.currency] / currencyRates['USD']));
            const dollarValue = calculatedValue * (blockChainPrice * (currencyRates['USD'] / currencyRates['USD']));
            console.log('DOLLAR AMOUNT: ', dollarValue, 'SELECTED CURRENCY: ', calculatedValue);
            setCurrencyEquivalent((oldState) => ({
                ...oldState,
                dollarValue,
            }))
            setAmountOfCoin({ value: calculatedValue, error: amountOfCoin.error });
        }
    }

    const handleAmountOfCoinChange = (e) => {
        setAmountOfCoin({ value: e.target.value, error: amountOfCoin.error });
        if (currencyRates['USD'] && currencyRates[currencyEquivalent.currency]) {
            const calculatedValue = (blockChainPrice * (currencyRates[currencyEquivalent.currency] / currencyRates['USD'])) * +e.target.value;
            const dollarValue = (blockChainPrice * (currencyRates['USD'] / currencyRates['USD'])) * +e.target.value;
            setCurrencyEquivalent((oldState) => ({
                ...oldState,
                amount: `${(calculatedValue)}`,
                dollarValue,
            }))
        }
    }

    const handleModalClose = () => {
        setShowModal((oldState) => ({
            ...oldState,
            tab: 0,
            value: false,
        }))
    }



    const handlePurchase = () => {
        console.log(amountOfCoin.value, 'amountOfCoin', currencyEquivalent.value)
        if (!amountOfCoin.value) {
            handleErrorMessageSetting("Amount of coin must be specified", setAmountOfCoin);
            return;
        }
        if (+amountOfCoin.value < 20000) {
            handleErrorMessageSetting("Minimum amount of 20,000 QC for ICO", setAmountOfCoin);
            return;
        }
        

        handleErrorMessageSetting("", setAmountOfCoin)
        handleErrorMessageSetting("", setCurrencyEquivalent)
        setShowModal((oldState) => ({
            ...oldState,
            tab: 0,
            value: true,
        }))
    }

    return (
        <div className="flex flex-col items-center">
            <Nav title="ICO" />
            <div className="bg_light_green flex w-full justify-center pb-20 pt-10">
                <h1 className="max_600 md:px-5 pt-20 ico_title">
                    Be one of the first buyers of <span className="text_lids_green_second italic">quicoin</span>, we are changing the world's finance
                </h1>
            </div>

            <div className="py-10 md:py-0 bg_lids_green w-full md:h-40 flex flex-col md:flex-row items-center justify-center">
                <FormInput
                    value={Number(amountOfCoin.value).toString()}
                    error={amountOfCoin.error}
                    label={"Amount of coin"}
                    type="number"
                    onChange={handleAmountOfCoinChange}
                />
                <FormInput
                    onChange={handleCurrencyEquivalentAmount}
                    type="number"
                    value={Number(currencyEquivalent.amount).toString()}
                    currency={currencyEquivalent.currency}
                    error={currencyEquivalent.error}
                    label={"Equivalent"}
                    allCurrencies={allCurrencies}
                    onCurrencyChange={onCurrencyChange}
                />
                <button className="submit_btn text-white w-10/12 md:w-32 py-3 rounded-md mt-4 mr-3" type="button" onClick={handlePurchase} >BUY</button>
            </div>
            {
                showModal.value === true &&
                <div
                    className="fixed top-0 bottom-0 right-0 left-0 bg-gray-900 bg-opacity-75 flex flex-col justify-center items-center">
                    <div className="modal_cancel_container flex justify-end">
                        <img onClick={handleModalClose} src="/assets/images/close_btn.svg" alt="close button" />
                    </div>
                    <div className="relative pb-5 w-11/12 md:w-2/3 bg-white rounded-2xl modal_white_inner">
                        <StepperHeader currentIndex={showModal.tab} />
                        {showModal.error && <p className="text-sm text-red-400 text-center">{showModal.error}</p>}                        {
                            // showModal.value === true && 
                            <div className="flex justify-center py-5">
                                {
                                    showModal.tab === 0 ?
                                        <CreateWallet setShowModal={setShowModal} /> :
                                        showModal.tab === 1 ?
                                            <PaymentInformation
                                                amount={Number((+currencyEquivalent.amount).toFixed(2))}
                                                dollarValue={Number((+currencyEquivalent.dollarValue).toFixed(2))}
                                                currency={currencyEquivalent.currency}
                                                qcAmount={amountOfCoin.value}
                                                walletAddress={showModal.walletAddress}
                                                setShowModal={setShowModal}
                                            /> :
                                            showModal.tab === 2 ?
                                                <Finish
                                                    blockChainPrice={blockChainPrice}
                                                    walletAddress={showModal.walletAddress}
                                                    setShowModal={setShowModal}
                                                />
                                                : null
                                }
                            </div>
                        }
                    </div>
                </div>
            }

        </div>
    )
}

export default ICO
