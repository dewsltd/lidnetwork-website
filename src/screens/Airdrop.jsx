import React, { useState } from 'react'
import Nav from '../components/Nav'
import Completed from './airdrop/Completed'
import CreateWallet from './airdrop/CreateWallet'
import GetCode from './airdrop/GetCode'
import Todos from './airdrop/Todos'
import Welcome from './airdrop/Welcome'
import "./css/Airdrop.scss"

const Airdrop = () => {
    const [currentScreen, setCurrentScreen] = useState({ page: 0, walletAddress: "", email: "" })
    return (
        <div id="airdrop_section">
            <Nav title="Airdrop" />
            <div className="flex-1 px-10">
                {
                    currentScreen.page === 0 ? <Welcome setCurrentScreen={setCurrentScreen} /> :
                        currentScreen.page === 1 ? <Todos setCurrentScreen={setCurrentScreen} /> :
                            currentScreen.page === 2 ? <GetCode setCurrentScreen={setCurrentScreen}  /> :
                                currentScreen.page === 3 ? <CreateWallet setCurrentScreen={setCurrentScreen} currentScreen={currentScreen} /> :
                                    currentScreen.page === 4 ? <Completed currentScreen={currentScreen} /> :
                                        null
                }
            </div>
        </div>
    )
}

export default Airdrop
