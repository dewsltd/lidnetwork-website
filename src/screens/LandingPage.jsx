import React from 'react';
import { Link } from 'react-router-dom';
import AboutCard from '../components/AboutCard';
import ChannelCard from '../components/ChannelCard';
import ProductCard from '../components/ProductCard';
import TopSection from '../components/TopSection';
import { developerNetwork, features, topReasons, aboutLID } from '../utils/data';
import Footer from '../components/Footer';
import Market from '../components/Market';

const LandingPage = () => {
    return (
        <div className="bg_lids_white min-h-screen relative overflow-hidden">
            <TopSection />
            <div id="description" className="bg_lids_white flex justify-center items-center px-2 my-20 md:my-36">
                <h1 className="w-full md:w-8/12 p-2 text_lids_gray text md:text-5xl xl:text-7xl 2xl:text-8xl 3xl:text-9xl" >Quicoin; facilitating global trade</h1>
            </div>
            <div id="features" className="bg_lids_gray flex flex-col md:flex-row flex-wrap p-20 md:pl-32 xl:pl-52">
                {features.map(item => (
                    <ProductCard
                        product={item}
                        key={item.title}
                    />
                ))}
            </div>
            <div id="developers" className="bg_lids_white flex flex-col md:flex-row p-10">
                <div className="w-full md:w-1/2 flex">
                    <img className="w-full" src="/assets/images/developers.svg" alt="developers section" />
                </div>
                <div className="w-full md:w-1/2 md:p-20">
                    <h1 className="text_lids_green text-2xl lg:text-5xl 2xl:text-7xl 3xl:text-9xl font-semibold mb-5">Developers</h1>
                    <p className="text-black w-full md:w-5/6 mb-5 md:mb-10 text-md lg:text-2xl 2xl:text-5xl 3xl:text-8xl">You can join our community and contribute to the blockchain project.</p>
                    {developerNetwork.map(network => (
                        <ChannelCard
                            key={network.title}
                            channelInfo={network}
                        />
                    ))}
                </div>
            </div>
            <div id="reason" className="bg_lids_white flex flex-col items-start w-full px-5 md:px-32 my-40">
                <h5 className="text_lids_green text-3xl mb-3" >Why we created Quicoin</h5>
                <h1 className="text_lids_gray createdReason xl:w-9/12"> We created Quicoin to solve the current financial issues in the world </h1>
                <div className="md:flex w-full mt-10 px-12 md:px-0">
                    {topReasons.map(item => (
                        <ProductCard
                            type="reason"
                            product={item}
                            key={item.title}
                        />
                    ))}
                </div>
            </div>
            <div id="paper" className="flex flex-col-reverse md:flex-row">
                <div className="md:w-1/2 p-5 flex justify-center items-start">
                    <img src="/assets/images/book.svg" className="w-10/12 md:w-1/2 white_paper_book" alt="litcoin white paper" />
                </div>
                <div className="md:w-1/2 flex flex-col justify-center items-start px-5 md:px-0">
                    <h1 className="text_lids_green text-3xl lg:text-4xl 2xl:text-5xl 3xl:text-8xl px-3 md:px-0">The White Paper</h1>
                    <p className="text-black mb-10 text-xl ml-3 mt-4">Introduction. Network Type.</p>
                    <a rel="noreferrer" target="_blank" href="/assets/pdf/quicoin_whitepaper.pdf" download className="ml-3 flex white_paper_link justify-between">
                        <p className="font-bold">Download the White Paper</p>
                        <img src="/assets/images/keyboard_backspace.svg" alt="go to white paper" />
                    </a>
                </div>
            </div>
            <Market />
            <div className="bg_lids_gray pt-20" id="about">
                <h1 className="text_lids_green text_size_35 px-5 md:px-20">About Quicoin</h1>
                <div className="w-full p-5 md:p-20 md:flex flex-wrap justify-between">
                    {aboutLID.map(item => (
                        <AboutCard
                            item={item}
                            key={item.title}
                        />
                    ))}
                </div>
            </div>
            <div className="bg_lids_gray w-full p-5 md:p-16 flex item-center justify-center">
                <div className="w-full developer_section bg_lids_green_light flex flex-col md:flex-row p-5">
                    <div className="w-1/2 self-center p-10">
                        <h1 className="text-2xl lg:text-4xl 2xl:text-5xl 3xl:text-8xl text-white">Developers</h1>
                        <p className="text-white w-full mt-5 mb-10 text-md lg:text-2xl 2xl:text-4xl 3xl:text-7xl">
                            Build products with Quicoin as a payment option. 
                            Users want to pay with Quicoin. Is your company accepting Quicoin?                        
                        </p>
                        <Link to="/documentation" className="flex  developer_link border-b-2 border-white justify-between">
                            <p>Learn More</p>
                            <img src="/assets/images/arrow_white.svg" alt="go to developer documentation" />
                        </Link>
                    </div>
                    <img className="w-72 md:w-6/12 developer_img self-center pr-10" src="/assets/images/developer.svg" alt="developer documentation" />
                </div>
            </div>
            <Footer />
        </div>
    )
}

export default LandingPage
