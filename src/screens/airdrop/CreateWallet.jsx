import React, { useState } from 'react'
import ContinueButton from '../../components/ContinueButton'
import FormInput from '../../components/FormInput'
import { blockchainApiCall, dilsApiCall } from '../../utils/apiCall';

const CreateWallet = ({ setCurrentScreen, currentScreen }) => {

    const [walletName, setWalletName] = useState({ value: "", error: "" });
    const [password, setPassword] = useState({ value: "", error: "" });
    const [confirmPassword, setConfirmPassword] = useState({ value: "", error: "" });
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState("");

    const displayError = (error) => {
        setError(error);
        setIsLoading(false);
        return;
    }

    const handleCreateWallet = async () => {
        try {
            setError("")
            setWalletName(oldState => ({ ...oldState, error: "" }))
            setPassword(oldState => ({ ...oldState, error: "" }))
            setConfirmPassword(oldState => ({ ...oldState, error: "" }))

            //Checks and Validations
            if (!currentScreen?.email) {
                return displayError("Email validation error");
            }
            if (!walletName.value) {
                setWalletName(oldState => ({ ...oldState, error: "Specify Wallet Name" }))
                return;
            }
            if (!password.value || password.value.length < 6) {
                setPassword(oldState => ({ ...oldState, error: "Password cannot be less than 6 characters" }))
                return;
            }
            if (password.value !== confirmPassword.value) {
                setConfirmPassword(oldState => ({ ...oldState, error: "Password doesn't match" }))
                return;
            }
            // Create Wallet
            setIsLoading(true);
            const requestModel = {
                private_key: password.value,
                wallet_name: walletName.value
            }
            const networkRequest = await blockchainApiCall.post('wallet/create', requestModel);
            console.log(networkRequest, 'CREATE WALLET REQUEST')
            if (networkRequest.status === "success" && networkRequest.address) {
                // If Wallet if created successfully, then credit it;
                const creditModel = { email: currentScreen.email, wallet_address: networkRequest.address };
                const creditRequest = await dilsApiCall.post(`callbacks/airdrop/release`, creditModel);
                console.log(creditRequest, 'CREDIT WALLET REQUEST')
                if (creditRequest.status === "success") {
                    setIsLoading(false)
                    setCurrentScreen((oldState) => ({
                        ...oldState,
                        page: 4,
                        walletAddress: networkRequest.address
                    }))
                    return;
                }
                return displayError(creditRequest?.message ?? "Error crediting wallet")
            }
            return displayError(networkRequest?.message ?? "Error creating wallet")
        } catch (error) {
            return displayError(error?.message ?? error?.data?.message ?? "Network Error")
        }
    }

    return (
        <div className="mt-28">
            { error && <p className="text-sm italics text-red-400 mb-5">{error}</p>}
            <FormInput
                isOutlineInput={true}
                value={walletName.value}
                error={walletName.error}
                className="md:w-5/12"
                label={"Input Wallet Name"}
                onChange={(e) => setWalletName({ value: e.target.value, error: walletName.error })}
            />
            <FormInput
                isOutlineInput={true}
                value={password.value}
                error={password.error}
                className="md:w-5/12"
                label={"Enter Password"}
                type="password"
                onChange={(e) => setPassword({ value: e.target.value, error: password.error })}
            />
            <FormInput
                isOutlineInput={true}
                value={confirmPassword.value}
                error={confirmPassword.error}
                className="md:w-5/12"
                type="password"
                label={"Re-enter Password"}
                onChange={(e) => setConfirmPassword({ value: e.target.value, error: confirmPassword.error })}
            />
            <ContinueButton
                isLoading={isLoading}
                text="Get Airdrop"
                className="md:mt-3"
                onPress={handleCreateWallet}
            />

        </div>
    )
}

export default CreateWallet
