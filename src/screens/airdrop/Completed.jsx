import React from 'react'
import { useHistory } from "react-router-dom";

import ContinueButton from '../../components/ContinueButton'
import FormInput from '../../components/FormInput'

const Completed = ({currentScreen}) => {

    const history = useHistory();

    return (
        <div className="pt-20">
            <p className="text-md text_form_gray italic mb-10 roboto w_400">
                Please copy this wallet address to access your coins. This will enable you to add a wallet on the Dils trading platform
            </p>
            <FormInput
                isOutlineInput={true}
                className="md:w-5/12"
                value={currentScreen?.walletAddress ?? "NO WALLET ADDRESS"}
                label={"Wallet Address"}
                onChange={(e) => { }}
                copyBtn={true}
            />
            <ContinueButton
                onPress={() => history.push("/")}
                text="Back to Quicoin Homepage"
            />

            <p className="mt-8 mb-2 text-md text_form_gray roboto">
                Easy download on:
            </p>
            <div className="flex flex-row download_app pt-2">
                <a rel="noreferrer" target="_blank" href="https://play.google.com/store/apps/details?id=com.dils"><img className="mr-4" src="/assets/images/google.svg" alt="google app download" /> </a>
                <a rel="noreferrer" target="_blank" href="https://apps.apple.com/us/app/dils/id1559896992"><img className="" src="/assets/images/apple.svg" alt="app store app download" /> </a>
            </div>
        </div>
    )
}

export default Completed
