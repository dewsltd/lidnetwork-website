import React from 'react'
import ContinueButton from '../../components/ContinueButton'

const Welcome = ({ setCurrentScreen }) => {

    const navigateToTodo = () => {
        setCurrentScreen((oldState) => ({
            ...oldState,
            page: 1,
        }))
    }

    return (
        <div className="airdrop_welcome">
            <h1 className="text_lids_green_second pt-20 pb-5 aircoin_title">
                Welcome to Quicoin Airdrop.
            </h1>
            <p className="welcome_text">
                You will need to perform a few tasks like following us on our social media to get free coins in your wallet
            </p>
            <ContinueButton text="Continue" onPress={navigateToTodo} className="mt-5" />
        </div>
    )
}

export default Welcome
