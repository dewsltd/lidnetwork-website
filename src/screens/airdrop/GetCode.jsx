import React, { useState } from 'react'
import ContinueButton from '../../components/ContinueButton'
import FormInput from '../../components/FormInput'
import { dilsApiCall } from '../../utils/apiCall';

const emailRegex = /\S+@\S+\.\S+/;

const GetCode = ({ setCurrentScreen }) => {
    const [email, setEmail] = useState({ value: "", error: "" })
    const [code, setCode] = useState({ value: "", error: "" })
    const [codeSent, setCodeSent] = useState(false)
    const [loaderAndError, setLoaderErrorHandler] = useState({ loader: false, error: "" })

    const handleGetCode = async () => {
        try {
            if (email.value === "" || !emailRegex.test(email.value)) {
                setLoaderErrorHandler({ error: "Please Input a valid email", loader: false });
                return;
            }
            setLoaderErrorHandler({ error: "", loader: true })

            const requestModal = { email: email.value };
            const networkRequest = await dilsApiCall.post(`callbacks/airdrop`, requestModal);
            if (networkRequest.status === "success") {
                setLoaderErrorHandler(oldState => ({ ...oldState, loader: false }))
                setCodeSent(true)
                return;
            }
            setLoaderErrorHandler({ error: networkRequest?.error ?? "Network Error", loader: false });
        } catch (error) {
            setLoaderErrorHandler({ error: error?.message ?? error?.data?.message ?? "Network Error", loader: false });
        }
    }

    const handleCodeValidation = async () => {
        try {
            setEmail(oldState => ({ ...oldState, error: "" }))
            setCode(oldState => ({ ...oldState, error: "" }))

            if (email.value === "" || !emailRegex.test(email.value)) {
                setEmail(oldState => ({ ...oldState, error: "Invalid Email" }))
                return;
            }
            if (code.value === "" || code.value.length !== 4) {
                setCode(oldState => ({ ...oldState, error: "Invalid Code" }))
                return;
            }

            setLoaderErrorHandler({ error: "", loader: true });

            const requestModal = { email: email.value, code: +code.value };
            const networkRequest = await dilsApiCall.post(`callbacks/airdrop/verify`, requestModal);
            if (networkRequest.status === "success") {
                setLoaderErrorHandler(oldState => ({ ...oldState, loader: false }))
                setCurrentScreen((oldState) => ({
                    ...oldState,
                    email: email.value,
                    page: 3,
                }))
                return;
            }
            setLoaderErrorHandler({ error: networkRequest?.error ?? "Network Error", loader: false });
        } catch (error) {
            setLoaderErrorHandler({ error: error?.message ?? error?.data?.message ?? "Network Error", loader: false });
        }
    }

    return (
        <div className="mt-28">
            { loaderAndError.error && <p className="text-sm italics text-red-400 mb-5">{loaderAndError.error}</p>}
            <FormInput
                isOutlineInput={true}
                value={email.value}
                error={email.error}
                type="email"
                className="md:w-5/12"
                label={"Enter Email"}
                onChange={(e) => setEmail({ value: e.target.value, error: email.error })}
            />

            { codeSent ?
                <>
                    <FormInput
                        isOutlineInput={true}
                        value={code.value}
                        error={code.error}
                        type="number"
                        className="md:w-5/12"
                        label={"Enter Code"}
                        onChange={(e) => setCode({ value: e.target.value, error: code.error })}
                    />
                    <ContinueButton
                        isLoading={loaderAndError.loader}
                        text="Continue"
                        className="md:mt-3"
                        onPress={handleCodeValidation}
                    />
                </>
                :
                <ContinueButton
                    isLoading={loaderAndError.loader}
                    text="Get Code"
                    className="md:mt-3"
                    onPress={handleGetCode}
                />
            }
        </div>
    )
}

export default GetCode
