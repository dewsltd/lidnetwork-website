import React, { useState } from 'react'
import ContinueButton from '../../components/ContinueButton';

const taskList = [
    { key: "twitter", status: false, title: "Follow us on Twitter", url: "https://twitter.com/Dilsmarket" },
    { key: "telegram", status: false, title: "Join out telegram", url: "https://t.me/joinchat/Rcq3QoXLmzQwMmZk" },
    { key: "tweet", status: false, title: "Tweet at us", url: "https://twitter.com/intent/tweet?text=@Dilsmarket" },
    { key: "discord", status: false, title: "Join our discord", url: "https://discord.gg/HsjZV3ptvE" },
]

const Item = ({ task, onPress }) => {
    const { status, url, title } = task;
    return <a href={url} target="__blank" onClick={onPress} className="flex items-center my-5 cursor-pointer">
        <div className={`radio_btn ${status ? 'bg_lids_green_light' : 'bg-white'}`} />
        <p className="task_text">{title}</p>
    </a>
}

const Todos = ({ setCurrentScreen }) => {

    const [allTaskList, setAllTaskList] = useState(taskList)
    const [error, setError] = useState("")

    const markItemAsDone = (item) => {
        setError("")
        item.status = true
        const otherItems = allTaskList.filter(task => task.key !== item.key);
        setAllTaskList([item, ...otherItems])
    }

    const navigateToGetCode = () => {
        const itemsLeft = allTaskList.filter(item => item.status === false);
        if (itemsLeft.length > 0) {
            setError(`Complete all tasks. You have ${itemsLeft.length} tasks left`);
            return;
        }
        setCurrentScreen((oldState) => ({
            ...oldState,
            page: 2,
        }));
    }

    return (
        <div className="airdrop_todo mt-32">
            { error && <p className="text-sm italics text-red-400">{error}</p>}
            {
                allTaskList.map(item => (
                    <Item
                        key={item.key}
                        task={item}
                        onPress={() => markItemAsDone(item)}
                    />
                ))
            }
            <ContinueButton
                text="Continue"
                onPress={navigateToGetCode}
            />
        </div>
    )
}

export default Todos
