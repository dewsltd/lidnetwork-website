import React, { useState, useEffect, Fragment } from 'react'
import FormInput from '../../components/FormInput';
import { blockchainApiCall } from '../../utils/apiCall';
import { displayModalError } from '../../utils/helpers';

const Finish = ({ setShowModal, blockChainPrice, walletAddress }) => {

    const [viewWallet, setViewWallet] = useState(false)
    const [walletInfo, setWalletInfo] = useState({})

    useEffect(() => {
        navigator.clipboard.writeText(walletAddress);
        const fetchWalletInfo = async () => {
            try {
                const networkRequest = await blockchainApiCall.post("wallet/info", { address: walletAddress });
                if (networkRequest.data) {
                    setWalletInfo(networkRequest.data);
                    return;
                }
                displayModalError(networkRequest, setShowModal, null, "Error fetching wallet balance. Please try again later")
            } catch (error) {
                displayModalError(error, setShowModal, null, "Network Error")
            }
        }

        fetchWalletInfo()
    }, [walletAddress, setShowModal]);


    const handleReturnHome = () => {
        setShowModal((oldState) => ({
            ...oldState,
            tab: 0,
            value: false,
        }))
    }

    return (
        <Fragment>
            {
                walletInfo?.balance ?
                    <>
                        {
                            viewWallet ?
                                <div className="w-full px-10">
                                    <p className="text-sm text_lids_green text-center">
                                        Please copy this wallet address, this will enable you to add a wallet on the Dils trading platform
                                    </p>
                                    <FormInput
                                        isOutlineInput={true}
                                        value={walletAddress}
                                        label={"Wallet Address"}
                                        onChange={(e) => { }}
                                        copyBtn={true}
                                        className="md:mb-0"
                                    />

                                    <div className="mt-10 w-full flex justify-center">
                                        <button className="bg_lids_green text-white py-2 rounded-3xl w-full" type="button" onClick={handleReturnHome}>
                                            BACK TO HOMEPAGE
                                         </button>
                                    </div>

                                    <p className={`text_form_gray text-sm font-semibold pt-5`}>Easy Download on: </p>
                                    <div className="flex flex-row download_app pt-2">
                                        <a rel="noreferrer" target="_blank" href="https://play.google.com/store/apps/details?id=com.dils"><img className="mr-4" src="/assets/images/google.svg" alt="google app download" /> </a>
                                        <a rel="noreferrer" target="_blank" href="https://apps.apple.com/us/app/dils/id1559896992"><img className="" src="/assets/images/apple.svg" alt="app store app download" /> </a>
                                    </div>
                                </div>
                                :
                                <div className="w-full px-10">
                                    <p className="text-md text_lids_green text-center mb-3">
                                        Your transaction is successful
                                    </p>

                                    <p className={`text_form_gray text-sm font-semibold`}>Your Total Wallet Balance</p>
                                    <div className="flex items-end">
                                        <p className="text-xl text_lids_green">
                                            {walletInfo?.balance ?? 0}QC
                                        </p>
                                        <small className="ml-3 mb-1 text-xs text_stepper_gray">
                                            ${walletInfo.balance * blockChainPrice}
                                        </small>
                                    </div>
                                    <div className="mt-10 w-full flex justify-center">
                                        <button className="bg_lids_green text-white py-2 rounded-3xl w-full" type="button" onClick={() => setViewWallet(true)}>
                                            CONTINUE
                                         </button>
                                    </div>

                                    <p className="pt-10 text-sm text_lids_green mb-3">
                                        Continue to the next stage to get your wallet address
                                    </p>

                                </div>
                        }
                    </>
                    :
                    <div className="w-full py-10 flex justify-center items-center">
                        <div className="loader">Loading...</div>
                    </div>

            }
        </Fragment>
    )
}

export default Finish
