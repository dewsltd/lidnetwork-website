import React, { Fragment, useState } from 'react'
import FormInput from '../../components/FormInput';
import { blockchainApiCall } from '../../utils/apiCall';
import { displayModalError } from '../../utils/helpers';

const CreateWallet = ({ setShowModal }) => {

    const [walletName, setWalletName] = useState({ value: "", error: "" });
    const [walletAddress, setWalletAddress] = useState({ value: "", state: false });
    const [password, setPassword] = useState({ value: "", error: "" });
    const [confirmPassword, setConfirmPassword] = useState({ value: "", error: "" });
    const [isLoading, setIsLoading] = useState(false);

    const navigateToPaymentPage = () => {
        setShowModal((oldState) => ({
            ...oldState,
            error: "",
            walletAddress: walletAddress.value,
            tab: 1,
        }))
    }

    const handleCreateWallet = async () => {
        try {
            setIsLoading(true);
            displayModalError("", setShowModal, null, "")
            //Checks and Validations
            if (password.value !== confirmPassword.value) {
                displayModalError({}, setShowModal, setIsLoading, "Password do not match");
                return;
            }
            // MAKE API CALL TO GENERATE WALLET ID
            const requestModel = {
                private_key: password.value,
                wallet_name: walletName.value
            }
            const networkRequest = await blockchainApiCall.post('wallet/create', requestModel);
            // console.log(networkRequest, 'CREATE WALLET REQUEST')
            if (networkRequest.status === "success" && networkRequest.address) {
                setWalletAddress({
                    state: true,
                    value: networkRequest?.address ?? 'NoWalletCreated',
                })
                return;
            }
            return displayModalError(networkRequest, setShowModal, setIsLoading, "Error Creating Wallet");
        } catch (error) {
            displayModalError(error, setShowModal, setIsLoading, "Network Error")
        }
    }

    return (
        <Fragment>
            {
                walletAddress.state === true ?
                    <div className="w-10/12">
                        <p className="text-md text_lids_green text-center mb-10">
                            Wallet successfully created
                        </p>
                        <FormInput
                            isOutlineInput={true}
                            value={walletAddress.value}
                            label={"Wallet Address"}
                            onChange={(e) => { }}
                            copyBtn={true}
                        />
                        <div className="w-full flex justify-center">
                            <button className="bg_lids_green text-white py-3 rounded-md mt-4 w-full" type="button" onClick={navigateToPaymentPage}>CONTINUE</button>
                        </div>
                    </div>
                    :
                    <div className="w-10/12">
                        <FormInput
                            isOutlineInput={true}
                            value={walletName.value}
                            error={walletName.error}
                            label={"Wallet Name"}
                            onChange={(e) => setWalletName({ value: e.target.value, error: walletName.error })}
                        />
                        <FormInput
                            isOutlineInput={true}
                            type="password"
                            value={password.value}
                            error={password.error}
                            label={"Enter Password(Minimum of 6 characters)"}
                            onChange={(e) => setPassword({ value: e.target.value, error: password.error })}
                        />
                        <FormInput
                            isOutlineInput={true}
                            type="password"
                            value={confirmPassword.value}
                            error={confirmPassword.error}
                            label={"Confirm Password"}
                            onChange={(e) => setConfirmPassword({ value: e.target.value, error: confirmPassword.error })}
                        />
                        <div className="h-10 mb-10">
                            {isLoading ?
                                <div className="loader">Loading...</div>
                                :
                                <div className="w-full flex justify-center">
                                    <button
                                        disabled={!(walletName.value && password.value.length > 5 && confirmPassword.value)}
                                        className={`${walletName.value && password.value.length > 5 && confirmPassword.value ? 'bg_lids_green' : 'bg-gray-400 cursor-not-allowed'} text-white py-3 rounded-3xl mt-4 w-full`} 
                                        type="button" 
                                        onClick={handleCreateWallet}>CREATE WALLET</button>
                                </div>
                            }
                        </div>
                    </div>}
        </Fragment>
    )
}

export default CreateWallet
