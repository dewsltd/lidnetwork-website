import React from 'react'
import PayPalButton from '../../components/PayPalButton'

const PaymentInformation = ({amount, currency, dollarValue, qcAmount, walletAddress, setShowModal}) => {
    return (
        <div className="mx-10 w-full">
            <p className="text-md text_lids_green text-center mb-10">
            {`You will be charged ${amount.toLocaleString()} ${currency} for this transaction($${dollarValue.toLocaleString()}) for the purchase of ${(Number(+qcAmount)).toLocaleString()}QC`}
            </p>
            <PayPalButton 
            amount={dollarValue} 
            currency={"USD"} 
            qcAmount={qcAmount}
            walletAddress={walletAddress}
            setShowModal={setShowModal}
            />
            <div id="paypal-button-container"></div>
        </div>
    )
}

export default PaymentInformation
